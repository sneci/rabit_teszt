<?php

namespace Db;

//Létrehozom a Database osztályt, hogy majd később tudjak queryket írni
class Database{

    private $servername = "localhost";
    private $username = "root";
    private $password = "";
    private $db = "rabit_teszt";
    private $conn = null;

    //kapcsolatot hozok létre osztálypéldányosításhoz konstruktor használatával
    public function __construct(){
        $this->conn = new \mysqli($this->servername, $this->username, $this->password, $this->db);
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
            }
        }
    
    //Megcsinálom a függvényt és a tömböt. a tömböt feltöltöm az sql táblában lévő adatokkal
    public function query($queryString){
        $sqldatas = array();
        $query=$this->conn->query($queryString);
        //row változóba belerakom a queryben lekért adatokat és átadom az sqldatasnak (erre fogok returnölni)
        while ($row = $query->fetch_assoc()){
            array_push($sqldatas, $row);
        }
        return $sqldatas;
    }
}

?>