<?php 

namespace Services;
use Db;
use Model;
include "Model/advertisementsModel.php";
require_once "Db/database.php";

class AdvertisementsService {
    private $database = null;
    private $userService = null;

    public function __construct() {
        $this->database = new Db\Database();
        $this->userService = new UsersService();
    }
    //Lekérem a tábla tartalmát és object-et csinálok belőle, majd ezzel feltöltöm a tömböt
    public function getAdvertisements() {
        $advertisements = $this->database->query('SELECT * from advertisements INNER JOIN users ON advertisements.userid=users.id');
        $advertisementsObject = array();
        foreach ($advertisements AS $advertisement) {
            $advertisementObject = new Model\AdvertisementsModel($advertisement['id'], $advertisement['userid'], $advertisement['title']);
            $userObject = $this->userService->getUserById($advertisement['userid']);
            $tempArray['advertisement'] = $advertisementObject;
            $tempArray['user'] = $userObject;
            $advertisementsObject[] = $tempArray;
        }
        return $advertisementsObject;
    }
}

?>