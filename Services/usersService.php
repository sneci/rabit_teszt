<?php 

namespace Services;
use Db;
use Model;
include "Model/usersModel.php";
require_once "Db/database.php";

class UsersService {
    private $database = null;

    public function __construct() {
        $this->database = new Db\Database();
    }
    //Lekérem a tábla tartalmát és object-et csinálok belőle, majd ezzel feltöltöm a tömböt
    public function getUsers() {
        $users = $this->database->query('SELECT * from users');
        $userObjects = array();
        foreach ($users AS $user) {
            $userObjects[] = new Model\UsersModel($user['id'], $user['name']);
        }
        return $userObjects;
    }

    //Ahhoz, hogy a user nevét hozzá tudjam kötni majd az advertisementhez le kell kérnem az id alapján a usernevet
    public function getUserById($id){
        $users = $this->database->query('SELECt * from users where id = '.$id);
        $userObject = null;
        foreach($users AS $user){
            $userObject = new Model\UsersModel($user['id'], $user['name']);
        }
        return $userObject;
    }
}

?>