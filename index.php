<?php

//betöltöm a classautoloadert
require_once 'classautoloader.php';
include "request.php";

//Switch - case megoldással választom ki éppen melyik controllert kell meghívni
$page = Request::GetPage();

switch($page){

    case "users":
        Controllers\usersController::Index();
        break;
    
    case "advertisements":
        Controllers\advertisementsController::Index();
        break;

    case "":
    case "/":
    case "home":
        Controllers\pageController::Index();
        break;

    default:
        Controllers\pageController::Error();
}

?>