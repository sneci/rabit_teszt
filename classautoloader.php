<?php

//betöltöm az összes classt az spl_autoload_register ami egy beépített függvény
spl_autoload_register(function($class){
    //A perjeleket cserélem, hogy ne kelljen dupla \\ használni
    $class = str_replace("\\","/", $class);
    if(file_exists($class .".php")){
        require_once $class .".php";
        } 
    }
);

?>