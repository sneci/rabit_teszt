<?php

//Létrehozok egy Request nevű classt, hogy a get paramétereket tudjam kezelni.
//A htmlspecialchars-t hozzátettem, hogyha speciális karaktert tartalmazna, biztos ami biztos, tudja kezelni
class Request{
   
    public static function Get($name){
        if(isset($_GET[$name])){
            return htmlspecialchars( $_GET[$name]);
        }
        return null;
    }

    //Page érték kiolvasására szolgáló függvény, nem kell az isset $_GET("page")-et használni
    public static function GetPage(){
        $page = self::Get("page");
        if(!$page) $page = "home";
        return $page;
    }

}

?>