<?php 

namespace Controllers;
use Services;
include "Services/usersService.php";

class UsersController {
    public static function Index() {
        $userService = new Services\UsersService();
        $users = $userService->getUsers();
        include "Views/usersView.php";
    }
}

?>