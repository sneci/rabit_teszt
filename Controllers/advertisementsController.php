<?php 

namespace Controllers;
use Services;
include "Services/advertisementsService.php";

class AdvertisementsController {
    public static function Index() {
        $advertisementsService = new Services\AdvertisementsService();
        $advertisements = $advertisementsService->getAdvertisements();
        include "Views/advertisementsView.php";
    }
}

?>